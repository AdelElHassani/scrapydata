DROP DATABASE IF EXISTS scrapy;
CREATE DATABASE scrapy;

USE scrapy;

CREATE TABLE product1 (
  id INT
    AUTO_INCREMENT
    NOT NULL,
  name VARCHAR(69)
    NOT NULL,
  reference VARCHAR(69)
    NOT NULL,
  price FLOAT
    NOT NULL,
  description TEXT
    NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE product2 (
  id INT
    AUTO_INCREMENT
    NOT NULL,
  name VARCHAR(69)
    NOT NULL,
  reference VARCHAR(69)
    NOT NULL,
  price FLOAT NOT NULL, discPrice FLOAT NOT NULL,
  description TEXT
    NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE product3 (
  id INT
    AUTO_INCREMENT
    NOT NULL,
  name VARCHAR(69)
    NOT NULL,
  reference VARCHAR(69)
    NOT NULL,
  price FLOAT
    NOT NULL,
  description TEXT
    NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE product4 (
  id INT
    AUTO_INCREMENT
    NOT NULL,
  name VARCHAR(69)
    NOT NULL,
  reference VARCHAR(69)
    NOT NULL,
  price FLOAT
    NOT NULL,
  description TEXT
    NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE client (
  id INT
    AUTO_INCREMENT
    NOT NULL,
  firstName VARCHAR(69)
    NOT NULL,
  lastName VARCHAR(69)
    NOT NULL,
  email VARCHAR(69)
    UNIQUE
    NOT NULL,
  phone VARCHAR(69)
    UNIQUE
    NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE tracktor (
  id INT AUTO_INCREMENT NOT NULL, stolenId INT NOT NULL,
  name VARCHAR(69)
    NOT NULL,
  minWeight INT NOT NULL, maxWeight INT NOT NULL,
  minLength INT NOT NULL, maxLength INT NOT NULL,
  minWidth INT NOT NULL, maxWidth INT NOT NULL,
  minCap INT NOT NULL, maxCap INT NOT NULL,
  energy VARCHAR(69)
    NOT NULL,
  description TEXT
    NOT NULL,
  PRIMARY KEY (id)
);